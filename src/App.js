import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import TitleComponent from './component/title/titleComponent';
import FormComponent from './component/form/formComponent';

function App() {
  return (
    <div>
      <TitleComponent/>
      <FormComponent/>
    </div>
  );
}

export default App;
