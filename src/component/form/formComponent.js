import { Component } from "react";

class FormComponent extends Component {
    onInputFirstNameClick(event) {
        console.log(event.target.value)
    }
    onInputLastNameClick(event) {
        console.log(event.target.value)
    }
    onInputCountryClick(event) {
        console.log(event.target.value)
    }
    onInputSubjectClick(event) {
        console.log(event.target.value)
    }
    onBtnSubmitClick() {
        console.log("Form đã được submit")
    }
    render() {
        return (
            <div className="jumbotron bg-light p-5 mt-5">
                <div className="row">
                    <div className="col-sm-4">
                        <label>First Name</label>
                    </div>
                    <div className="col-sm-8">
                        <input className="form-control" placeholder="Your name..." onChange={this.onInputFirstNameClick}/>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-4">
                        <label>Last Name</label>
                    </div>
                    <div className="col-sm-8">
                        <input className="form-control" placeholder="Your last name..." onChange={this.onInputLastNameClick}/>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-4">
                        <label>Country</label>
                    </div>
                    <div className="col-sm-8">
                        <select className="form-control" onChange={this.onInputCountryClick}>
                            <option value="Australia">Autralia</option>
                            <option value="USA">USA</option>
                            <option value="VietNam">VietNam</option>
                        </select>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-sm-4">
                        <label>Subject</label>
                    </div>
                    <div className="col-sm-8">
                        <textarea className="form-control" placeholder="Write something.." rows="5" cols="15" onChange={this.onInputSubjectClick}></textarea>
                    </div>
                </div>
                <div className="row mt-4">
                    <button className="btn btn-success w-25" type="submit" onClick={this.onBtnSubmitClick}>Send data</button>
                </div>
            </div>
        )
    }
}

export default FormComponent